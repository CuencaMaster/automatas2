import compiler.LexicalAnalyzer;
import compiler.Symbol;
/**
 * Author: Cuenca
 * Create at: 20/Septiembre/2020.
 */
public class Main {

    public static void main(String[] args) {
	    lexicalTest();
    }

    
    public static void lexicalTest() {
        LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer("test/prueba2");
        Symbol symbol = lexicalAnalyzer.getSymbol();

        System.out.println("Lista de todas las palabras：");
        String format = "%6s%12s%20s%16s%8s";
        System.out.format(format, "N.Token ", "Tipo", "Token", "Palabra", "Valor");
        System.out.println();
        for (int counter = 1; symbol.getSymbolType() != Symbol.PERIOD; counter++) {
            System.out.format (
                    format,
                    counter,
                    symbol.getSymbolType(),
                    symbol.getSymbolTypeName(),
                    symbol.getName(),
                    symbol.getNumber()
            );
            System.out.println();
            symbol = lexicalAnalyzer.getSymbol();
        }
        System.out.println();
    }
}
