package compiler;


public class Symbol {


    //public static final int SYMBOL_NUMBER = 35;       
  // public static final int NUMBER_MAX = 1000000;     



    
    public static final int NUL = 0;                  // NULL
    public static final int PLUS = 1;                 
    public static final int MINUS = 2;                
    public static final int MULTIPLY = 3;             
    public static final int DIVIDE = 4;               
    public static final int EQUAL = 5;               
    public static final int NOT_EQUAL = 6;           
    public static final int LESS = 7;                 
    public static final int LESS_OR_EQUAL = 8;        
    public static final int GREATER = 9;              
    public static final int GREATER_OR_EQUAL = 10;    
    public static final int LEFT_PARENTHESIS = 11;    
    public static final int RIGHT_PARENTHESIS = 12;   
    public static final int COMMA = 13;               
    public static final int SEMICOLON = 14;           
    public static final int PERIOD = 15;              
    public static final int BECOMES = 16;             


    public static final int IDENTIFIER = 17;


    /**------------------------------------
     * 
Solo hay enteros sin signo en la constante <entero sin signo> :: = <número> {<número>}
     * -------------------------------------
     */
    public static final int NUMBER = 18;


    /**------------------------------------
     * Palabras reservadas

     * const, var, procedure, odd,
     * if, then, else, while,
     * do, call, begin, end,
     * repeat, until, read, write
     * -------------------------------------
     */
    public static final int CONST_SYMBOL = 19;
    public static final int VAR_SYMBOL = 20;
    public static final int PROCEDURE_SYMBOL = 21;
    public static final int ODD_SYMBOL = 22;
    public static final int IF_SYMBOL = 23;
    public static final int THEN_SYMBOL = 24;
    public static final int ELSE_SYMBOL = 25;
    public static final int WHILE_SYMBOL = 26;
    public static final int DO_SYMBOL = 27;
    public static final int CALL_SYMBOL = 28;
    public static final int BEGIN_SYMBOL = 29;
    public static final int END_SYMBOL = 30;
    public static final int REPEAT_SYMBOL = 31;
    public static final int UNTIL_SYMBOL = 32;
    public static final int READ_SYMBOL = 33;
    public static final int WRITE_SYMBOL = 34;
  //  public static final int RESERVED_WORD= 35;

    // Configura los nombres de las palabras reservadas en orden alfabético para facilitar la búsqueda binaria
    public static final String[] WORD = new String[] {
            "begin", "call", "const", "do",
            "else", "end", "if", "odd",
            "procedure", "read", "repeat", "then",
            "until", "var", "while", "write"
    };

    // Valor de símbolo correspondiente a la palabra reservada
    public static final int[] WORD_SYMBOL = new int[] {
            BEGIN_SYMBOL, CALL_SYMBOL, CONST_SYMBOL, DO_SYMBOL,
            ELSE_SYMBOL, END_SYMBOL, IF_SYMBOL, ODD_SYMBOL,
            PROCEDURE_SYMBOL, READ_SYMBOL, REPEAT_SYMBOL, THEN_SYMBOL,
            UNTIL_SYMBOL, VAR_SYMBOL, WHILE_SYMBOL, WRITE_SYMBOL
    };



    private int symbolType;  // El tipo de símbolo, que es uno de los 35 anteriores.

    private int number = 0; // Si el signo es un entero sin signo, registra su valor

    private String name = ""; // Si el símbolo es una palabra reservada, registre su nombre

    public Symbol(int symbolType) {
        this.symbolType = symbolType;
    }


    /*------------------------------
     * Getter
     * -----------------------------
     */

    public String getName() {
        return name;
    }

    public int getSymbolType() {
        return symbolType;
    }

    public int getNumber() {
        return number;
    }



    /*------------------------------
     * Setter
     * -----------------------------
     */

    public void setNumber(int number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }



    /*-------------------
     * Debug
     * ------------------
     */

    public static final String[] TYPE_NAME = {
            "NULL",
            "+",
            "-",
            "*",
            "/",
            "=",
            "<>",
            "<",
            "<=",
            ">",
            ">=",
            "(",
            ")",
            ",",
            ";",
            ".",
            ":=",
            "identifier",
            "number",
            "const",
            "var",
            "procedure",
            "odd",
            "if",
            "then",
            "else",
            "while",
            "do",
            "call",
            "begin",
            "end",
            "repeat",
            "until",
            "read",
            "write"

    };

    
    public String getSymbolTypeName() {
        return TYPE_NAME[getSymbolType()];
    }
}
