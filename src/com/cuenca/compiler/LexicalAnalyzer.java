package compiler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

/**
 * Author: Cuenca
 * 
 */
public class LexicalAnalyzer {


    private BufferedReader sourceBuffer;
    private char currentChar = ' ';  

    private String currentLineString; 
    private int currentLineStringLength = 0;  
    private int currentLineStringIndex = 0;  

    
    private int[] simpleOperators;


    /**
     * Constructor
     *
     * @param filePath 
     */
    public LexicalAnalyzer(String filePath) {
        try {
            sourceBuffer = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("¡error! el archivo no existe");
        }

        // Inicializa operadores simples
        simpleOperators = new int[256]; 
        Arrays.fill(simpleOperators, Symbol.NUL); 
        simpleOperators['.'] = Symbol.PERIOD;
        simpleOperators[','] = Symbol.COMMA;
        simpleOperators[';'] = Symbol.SEMICOLON;
        simpleOperators['='] = Symbol.EQUAL;
        simpleOperators['+'] = Symbol.PLUS;
        simpleOperators['-'] = Symbol.MINUS;
        simpleOperators['('] = Symbol.LEFT_PARENTHESIS;
        simpleOperators[')'] = Symbol.RIGHT_PARENTHESIS;
        simpleOperators['*'] = Symbol.MULTIPLY;
        simpleOperators['/'] = Symbol.DIVIDE;
    }

    
    public Symbol getSymbol() {
        
        while (isSpace()) {
            getChar();
        }

        if (isLetter()) {
            
            return getKeywordOrIdentifier();
        } else if (isDigit()) {
           
            return getNumber();
        } else {
            
            return getOperator();
        }
    }


    
    private void getChar() {
        if (currentLineStringIndex == currentLineStringLength) {  
            try {
                String redString = "";

                
                while (redString.equals("")) {

                    redString = sourceBuffer.readLine();

                    if (redString == null) {
                       
                        System.out.println("EOF, ERROR!");
                        System.exit(0);
                    }

                    
                    redString = redString.trim();
                }

                currentLineString = redString;

               
                currentLineString = currentLineString.replaceAll("\t", " ");

                
                currentLineString += " ";

                currentLineStringLength = currentLineString.length();
                currentLineStringIndex = 0;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        currentChar = currentLineString.charAt(currentLineStringIndex);  
        currentLineStringIndex++;  
    }


    
    private boolean isSpace() {
        return (currentChar == ' ');
    }

    
    private boolean isDigit() {
        return currentChar >= '0' && currentChar <= '9';
    }

    
    private boolean isLetter() {
        return (currentChar >= 'a' && currentChar <= 'z') || (currentChar >= 'A' && currentChar <= 'Z');
    }

    


   
    private Symbol getKeywordOrIdentifier() {
        Symbol symbol;
        String token = "";  

        
        //Aqui se produce el null en caracteres especiales
        while (isLetter() || isDigit()) {
            token += currentChar;
            getChar();
        }

        
        int index = Arrays.binarySearch(Symbol.WORD, token.toLowerCase());

        if (index < 0) { 
            symbol = new Symbol(Symbol.IDENTIFIER);  //AQUI
            symbol.setName(token);
        } else { 
            symbol = new Symbol(Symbol.WORD_SYMBOL[index]);
        }

        return symbol;
    }

    
    private Symbol getNumber() {
        Symbol symbol = new Symbol(Symbol.NUMBER);
        int value = 0;
        do {
            value = value * 10 + (currentChar - '0');  
            getChar();
        } while (isDigit());

        symbol.setNumber(value); 

        return symbol;
    }

    
    private Symbol getOperator() {
        Symbol symbol;

        switch (currentChar) {
            case ':':
                getChar();
                if (currentChar == '=') { // Simbolo de asignacion :=
                    symbol = new Symbol(Symbol.BECOMES);
                    getChar();
                } else {
                    
                    symbol = new Symbol(Symbol.NUL);
                }
                break;
            case '<':
                getChar();
                switch (currentChar) {
                    case '>':  
                        symbol = new Symbol(Symbol.NOT_EQUAL);
                        getChar();
                        break;
                    case '=':  
                        symbol = new Symbol(Symbol.LESS_OR_EQUAL);
                        getChar();
                        break;
                    default:
                        symbol = new Symbol(Symbol.LESS);
                        break;
                }
                break;
            case '>':
                getChar();
                if (currentChar == '=') { 
                    symbol = new Symbol(Symbol.GREATER_OR_EQUAL);
                    getChar();
                } else { 
                    symbol = new Symbol(Symbol.GREATER);
                }
                break;
            default:
                
                symbol = new Symbol(simpleOperators[currentChar]);

                
                getChar();
                break;
        }

        return symbol;
    }
}
